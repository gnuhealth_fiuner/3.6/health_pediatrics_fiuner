from trytond.pool import Pool
from .health_pediatrics_fiuner import *

def register():
    Pool.register(
        NewBorn,
        module='health_pediatrics_fiuner', type_='model')


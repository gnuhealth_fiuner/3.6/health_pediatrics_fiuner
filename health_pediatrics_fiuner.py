# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from trytond.model import ModelView, ModelSingleton, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Equal, Eval, Bool, Not
from datetime import datetime, date, time


__all__ = ['NewBorn']

class NewBorn (metaclass = PoolMeta):    
    'New Born Data'
    __name__ = 'gnuhealth.newborn'
    
    born_week = fields.Integer('Born week')
    
    @staticmethod
    def default_born_week():
        return 37
    
    @fields.depends('patient')
    def on_change_with_name(self):
        if self.patient and self.patient.name.ref:
            return self.patient.name.ref
        
    @fields.depends('patient')
    def on_change_with_sex(self):
        if self.patient and self.patient.name.gender:
            return self.patient.name.gender
        
    @fields.depends('patient')
    def on_change_with_birth_date(self):
        if self.patient and self.patient.name.dob:
            return datetime.combine(self.patient.name.dob, time(12,0))
    father = fields.Many2One('gnuhealth.patient','Father')
    percentiles = ["less than P3","P3 y P10","P10 y P25",
                   "P25 y P50","P50 y P75","P75 y P90",
                   "P90 y P97","greater than P97"]
    exacts_percentiles = ["P3","P10","P25","P50","P75","P90","P97"]
    
    # PARA CALCULAR EL PERCENTIL DEL weight
    w_perc = fields.Function(fields.Char('Weight Percentile'),'get_w_perc')
    wboys_w = [[2.5,2.8,3.0,3.3,3.7,4.0,4.3],[2.6,2.9,3.2,3.5,3.8,4.2,4.5],
           [2.8,3.1,3.4,3.8,4.1,4.5,4.9],[3.1,3.4,3.7,4.1,4.5,4.8,5.2],
           [3.4,3.7,4.0,4.4,4.8,5.2,5.6],[3.6,3.9,4.3,4.7,5.1,5.5,5.9]]
    wgirls_w = [[2.4,2.7,2.9,3.2,3.6,3.9,4.2],[2.5,2.8,3.0,3.3,3.7,4.0,4.4],
            [2.7,3.0,3.2,3.6,3.9,4.3,4.6],[2.9,3.2,3.5,3.8,4.2,4.6,5.0],
            [3.1,3.4,3.7,4.1,4.5,4.9,5.3],[3.3,3.6,4.0,4.3,4.8,5.2,5.6]]
    
    def get_w_perc(self, patient):
        return self.on_change_with_w_perc()
    
    @fields.depends('patient','weight','wboys_w','wgirls_w',
                    'sex','born_week','exacts_percentiles',
                    'percentiles')
    def on_change_with_w_perc(self):
        age_w = 0
        correction = 0
        if self.patient and self.weight and self.born_week:
            correction = 37- int(self.born_week)
            age_w = 0-correction
            w = self.weight/1000
        if self.patient and age_w < 0 and age_w:
            w_perc = "Before term -> No established range"
            return w_perc
        elif self.patient and self.sex == "m" and age_w >=0 and age_w <= 5 and self.weight:
            if w in self.wboys_w[age_w]:
                w_perc = self.exacts_percentiles[self.wboys_w[age_w].index(w)]
                return w_perc
            else:
                aux = self.wboys_w[age_w]+ [float(w)]
                aux.sort()
                w_perc = self.percentiles[aux.index(w)]
                return w_perc
        elif self. patient and self.sex =="m" and age_w >5 and self.weight:
            w_perc = "Out of range"
            return w_perc
        elif self.patient and self.sex == "f" and age_w >=0 and age_w <= 5 and self.weight:
            if w in self.wgirls_w[age_w]:
                w_perc = self.exacts_percentiles[self.wgirls_w[age_w].index(w)]
                return w_perc
            else:
                aux = self.wgirls_w[age_w]+ [float(w)]
                aux.sort()
                w_perc = self.percentiles[aux.index(w)]
                return w_perc
        elif self.patient and self.sex =="f" and age_w >5 and self.weight and age_w:
            w_perc = "Out of range"
            return w_perc    
    
    # PARA CALCULAR EL PERCENTIL DE LA TALLA

    h_perc = fields.Function(fields.Char('Height Percentile'),'get_h_perc')
    
    hgirls_w = [[45.6,46.7,47.8,49.1,50.4,51.5,52.7],
            [46.8,47.9,49.0,50.3,51.6,52.7,53.9],
            [47.9,49.0,50.2,51.5,52.7,53.9,55.1],
            [48.8,50.0,51.1,52.5,53.7,54.9,56.1],
            [49.7,50.8,52.0,53.4,54.6,55.8,57.0],
            [50.5,51.7,52.9,54.2,55.5,56.7,57.9]]

    hboys_w = [[46.3,47.4,48.6,49.9,51.1,52.3,53.4],
           [47.5,48.6,49.8,51.1,52.3,53.5,54.7],
           [48.8,49.8,51.0,52.3,53.6,54.7,55.9],
           [49.8,50.9,52.0,53.4,54.6,55.8,57.0],
           [50.7,51.9,53.0,54.4,55.6,56.8,58.0],
           [51.7,52.8,54.0,55.3,56.6,57.8,59.0]]    
    
    def get_h_perc(self, patient):
        return self.on_change_with_h_perc()
    
    @fields.depends('patient','length','hboys_w','hgirls_w',
                    'sex','born_week','exacts_percentiles',
                    'percentiles')
    def on_change_with_h_perc(self):
        age_w = 0
        correction = 0        
        if self.patient and self.length and self.born_week:            
            correction = 37- int(self.born_week)
            age_w = 0-correction            
        if self.patient and age_w < 0 and age_w:
            h_perc = "Before term -> No established range"
            return h_perc
        elif self.patient and self.sex == "m" and age_w >=0 and age_w <= 5 and self.length:
            if self.length in self.hboys_w[age_w]:
                h_perc = self.exacts_percentiles[self.hboys_w[age_w].index(self.length)]
                return h_perc
            else:
                aux = self.hboys_w[age_w]+ [float(self.length)]
                aux.sort()
                h_perc = self.percentiles[aux.index(self.length)]
                return h_perc
        elif self. patient and self.sex =="m" and age_w >5 and self.length:
            h_perc = "Out of range"
            return h_perc
        elif self.patient and self.sex == "f" and age_w >=0 and age_w <= 5 and self.length:
            if self.length in self.hgirls_w[age_w]:
                h_perc = self.exacts_percentiles[self.hgirls_w[age_w].index(self.length)]
                return h_perc
            else:
                aux = self.hgirls_w[age_w]+ [float(self.length)]
                aux.sort()
                h_perc = self.percentiles[aux.index(self.length)]
                return h_perc
        elif self.patient and self.sex =="f" and age_w >5 and self.length and age_w:
            h_perc = "Out of range"
            return h_perc  

# PARA CALCULAR EL PERCENTIL DEL DIAMETRO CEFÁLICO
    cp_perc = fields.Function(fields.Char('Cefalic Perimeter Percentile'),'get_cp_perc')
    cpboys_w = [[32.1,32.8,33.6,34.5,35.3,36.1,36.9],[32.9,33.6,34.3,35.2,36.0,36.7,37.5],
            [33.7,34.4,35.1,35.9,36.7,37.4,38.1],[34.3,35.0,35.7,36.5,37.3,38.0,38.7],
            [34.9,35.6,36.3,37.1,37.9,38.6,39.3],[35.4,36.1,36.8,37.6,38.4,39.1,39.8]]
    cpgirls_w = [[31.7,32.4,33.1,33.9,34.7,35.4,36.1],[32.4,33.1,33.8,34.6,35.3,36.0,36.7],
             [33.1,33.8,34.5,35.2,36.0,36.7,37.4],[33.7,34.4,35.1,35.8,36.6,37.3,38.0],
             [34.2,34.9,35.6,36.4,37.2,37.9,38.6],[34.6,35.3,36.1,36.8,37.6,38.4,39.1]]
 
    def get_cp_perc(self, patient):
        return self.on_change_with_cp_perc()
    
    @fields.depends('patient','cephalic_perimeter','cpboys_w','cpgirls_w',
                    'sex','born_week','exacts_percentiles',
                    'percentiles')
    def on_change_with_cp_perc(self):
        age_w = 0
        correction = 0        
        if self.patient and self.cephalic_perimeter and self.born_week:            
            correction = 37- int(self.born_week)
            age_w = 0-correction
            
        if self.patient and age_w < 0 and age_w:
            cp_perc = "Before term -> No established range"
            return cp_perc
        elif self.patient and self.sex == "m" and age_w >=0 and age_w <= 5 and self.cephalic_perimeter:
            if self.cephalic_perimeter in self.cpboys_w[age_w]:
                cp_perc = self.exacts_percentiles[self.cpboys_w[age_w].index(self.cephalic_perimeter)]
                return cp_perc
            else:
                aux = self.cpboys_w[age_w]+ [float(self.cephalic_perimeter)]
                aux.sort()
                cp_perc = self.percentiles[aux.index(self.cephalic_perimeter)]
                return cp_perc
        elif self. patient and self.sex =="m" and age_w >5 and self.cephalic_perimeter:
            cp_perc = "Out of range"
            return cp_perc
        elif self.patient and self.sex == "f" and age_w >=0 and age_w <= 5 and self.cephalic_perimeter:
            if self.cephalic_perimeter in self.cpgirls_w[age_w]:
                cp_perc = self.exacts_percentiles[self.cpgirls_w[age_w].index(self.cephalic_perimeter)]
                return cp_perc
            else:
                aux = self.cpgirls_w[age_w]+ [float(self.cephalic_perimeter)]
                aux.sort()
                cp_perc = self.percentiles[aux.index(self.cephalic_perimeter)]
                return cp_perc
        elif self.patient and self.sex =="f" and age_w >5 and self.cephalic_perimeter and age_w:
            cp_perc = "Out of range"
            return cp_perc
